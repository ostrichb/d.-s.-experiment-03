//
// Created by Ostrichbeta Chan on 2022/03/17.
// Task 02: Comparable
// Description: Create a Robot class, and sort the class list with various sort rules and learn the Comparable object.

#include <iostream>
#include <string>
#include <array>
#include <utility>
#include <iomanip>
#include <ios>

class Robot {
private:
    int ID = 0;
    std::string botname;
    double IQ = 0.0;
public:
    Robot(int ID, std::string _name, double IQ): ID(ID), botname(std::move(_name)), IQ(IQ){};
    friend std::ostream& operator<< (std::ostream &os, const Robot &bot);
    [[nodiscard]] int getID() const{ return ID; };
    [[nodiscard]] std::string getName() const{ return botname; };
    [[nodiscard]] double getIQ() const{ return IQ; };
};

std::ostream &operator<<(std::ostream &os, const Robot &bot) {
    os.setf(std::ios::fixed);
    os << "{Robot|#"<< bot.getID() << ",name=" << bot.getName() << ",IQ:" << std::setprecision(1) << bot.getIQ() << std::setprecision(6) << "}";
    return os;
}

void task02Entry(){
    std::array<Robot, 10> robotList = {
            Robot(5, "Full Name", 86),
            Robot(9, "Rhys Hawkins", 96),
            Robot(2, "Vilma Jarvi", 203),
            Robot(0, "Ted Ellison", 45),
            Robot(3, "Heath Atwood", 66),
            Robot(1, "Kinslee Fink", 78),
            Robot(4, "Joshua Wilson", 91),
            Robot(7, "Victoria Roach", 79),
            Robot(8, "Ellis Schaefer", 103),
            Robot(6, "Regan Rosen", 30)
    };
    std::array<Robot, 10> initial(robotList);
    std::cout << "Unsorted: ";
    for (Robot &item : robotList) {
        std::cout << item << std::endl;
    }
    std::sort(robotList.begin(), robotList.end(), [](const Robot& a, const Robot& b){return a.getName() <= b.getName();});
    std::cout << "\nSorted by name: ";
    for (Robot &item : robotList) {
        std::cout << item << std::endl;
    }

    robotList = initial;
    std::sort(robotList.begin(), robotList.end(), [](const Robot& a, const Robot& b){return a.getID() <= b.getID();});
    std::cout << "\nSorted by ID: ";
    for (Robot &item : robotList) {
        std::cout << item << std::endl;
    }

    robotList = initial;
    std::sort(robotList.begin(), robotList.end(), [](const Robot& a, const Robot& b){return a.getIQ() <= b.getIQ();});
    std::cout << "\nSorted by IQ: ";
    for (Robot &item : robotList) {
        std::cout << item << std::endl;
    }
};
