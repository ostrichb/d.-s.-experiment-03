//
// Created by Ostrichbeta Yick-Ming Chan on 3/4/22.
//
#include <string>
#ifndef DSTASKPICKER_TASKMODULE_H
#define DSTASKPICKER_TASKMODULE_H

class TaskTemplate {
private:
    std::string title;
    std::string description;
public:
    TaskTemplate(std::string title, std::string description, void (*entry)());
    std::string getTitle();
    std::string getDescription();
    void (*entry)();
};


#endif //DSTASKPICKER_TASKMODULE_H
