//
// Created by Ostrichbeta Yick-Ming Chan on 3/17/22.
//

#ifndef WHUDATASTRUCTUREEXP03_TASK03_H
#define WHUDATASTRUCTUREEXP03_TASK03_H

#include "TaskTemplate.h"
#include <iterator>

void task03Entry();
const TaskTemplate task03("Sorting Algorithms", "Learn insert sort, bubble sort, quick sort and select sort.", task03Entry);

template <typename T, typename Predicate, size_t SIZE>
std::array<T, SIZE> insert_sort(std::array<T, SIZE> arr, Predicate p){
    std::array<T, SIZE> result {};
    size_t FilledAmount = 0;
    for (T& item : arr) {
        //Arrange the place of the element
        size_t CurrentPos = 0;
        while (CurrentPos < FilledAmount){
            if (p(item, result[CurrentPos])) break;
            CurrentPos++;
        }
        for (size_t i = SIZE - 1; i > CurrentPos; --i) {
            result[i] = result[i-1];
        }
        result[CurrentPos] = item;
        FilledAmount++;
    }
    return result;
}

template <typename T, typename Predicate, class Iterator>
void bubble_sort(Iterator begin, Iterator end, Predicate p) {
    size_t size = std::distance(begin, end);
    for (size_t i = 0; i < size; ++i) {
        for (size_t j = 0; j < size - i - 1; ++j) {
            if (!p(begin[j], begin[j + 1])) {
                T swap = begin[j];
                begin[j] = begin[j + 1];
                begin[j + 1] = swap;
            }
        }
    }
}

template <typename T, typename Iterator, typename Predicate>
void quick_sort(Iterator begin, Iterator end, Predicate p) {
    size_t size = std::distance(begin, end); // Get the current size
    switch(size) {
        case 0:
        case 1:
            return;

        case 2:
            if (!p(begin[0], begin[1])) {
                T swap = begin[0];
                begin[0] = begin[1];
                begin[1] = swap;
            }
            return;

        default:
            // For other situation, choose the last element as the pivot
            T pivot = begin[size - 1];
            std::vector<T> leftVector;
            std::vector<T> rightVector;
            for (int i = 0; i < size - 1; ++i) {
                if (p(begin[i], pivot)) {
                    leftVector.push_back(begin[i]);
                } else {
                    rightVector.push_back(begin[i]);
                }
            }

            // Sort the left and the right part manually
            quick_sort<T>(leftVector.begin(), leftVector.end(), p);
            quick_sort<T>(rightVector.begin(), rightVector.end(), p);

            // Finally, merge the left, pivot and the right together.
            std::vector<T> result {};
            for (T &item : leftVector) {
                result.push_back(item);
            }
            result.push_back(pivot);
            for (T &item : rightVector) {
                result.push_back(item);
            }

            // Copy the result to the original one
            std::copy(result.begin(), result.end(), begin);
    }

}

template <typename T, typename Iterator, typename Predicate>
void merge_sort(Iterator begin, Iterator end, Predicate p) {
    size_t size = std::distance(begin, end); // Get the current size
    switch(size) {
        case 0:
        case 1:
            return;

        case 2:
            if (!p(begin[0], begin[1])) {
                T swap = begin[0];
                begin[0] = begin[1];
                begin[1] = swap;
            }
            return;

        default:
            // For other situation, choose the last element as the pivot
            std::vector<T> leftVector(begin, begin + (size / 2));
            std::vector<T> rightVector(begin + (size / 2), end);

            // Sort the left and the right part manually
            merge_sort<T>(leftVector.begin(), leftVector.end(), p);
            merge_sort<T>(rightVector.begin(), rightVector.end(), p);

            // Finally, merge the left, pivot and the right together.
            std::vector<T> result {};
            for (T &item : leftVector) {
                result.push_back(item);
            }
            for (T &item : rightVector) {
                result.push_back(item);
            }

            // Copy the result to the original one
            std::copy(result.begin(), result.end(), begin);
    }

}

#endif //WHUDATASTRUCTUREEXP03_TASK03_H
