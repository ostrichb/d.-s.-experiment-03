//
// Created by Ostrichbeta Chan on 2022/03/17.
//

#ifndef WHUDATASTRUCTUREEXP03_TASK02_H
#define WHUDATASTRUCTUREEXP03_TASK02_H

#include "TaskTemplate.h"

void task02Entry();
const TaskTemplate task02("Comparable", "Create a Robot class, and sort the class list with various sort rules and learn the Comparable object.", task02Entry);

#endif //WHUDATASTRUCTUREEXP03_TASK02_H
