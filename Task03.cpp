//
// Created by Ostrichbeta Yick-Ming Chan on 3/17/22.
// Task 03: Sorting Algorithms
// Description: Learn insert sort, bubble sort, quick sort and select sort.

#include <array>
#include <iostream>
#include <random>

#include "Task03.h"

void task03Entry(){
    // Random number generator configuration
    std::random_device rd;
    std::mt19937_64 gen(rd());
    std::uniform_int_distribution dis(-99,99);

    std::array<int, 10> numList{};

    std::cout << "Unsorted: ";
    for (int &item : numList) {
        item = dis(gen); // Set a random number between -99 and 99 to the element
        std::cout << item << " ";
    }
    std::cout << std::endl;

    //numList = bubble_sort(numList, std::less_equal());
    bubble_sort<int>(numList.begin(), numList.end(), std::less_equal());

    std::cout << "Sorted: ";
    for (int &item : numList) {
        std::cout << item << " ";
    }
    std::cout << std::endl;
}

