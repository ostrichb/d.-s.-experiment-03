//
// Created by Ostrichbeta Chan on 2022/03/17.
// Task 01: Sorts and predicates
// Description: Make use of the std::sort function with predicates.

#include <random>
#include <algorithm>
#include <iostream>
#include <array>

void task01Entry(){
    // Random number generator configuration
    std::random_device rd;
    std::mt19937_64 gen(rd());
    std::uniform_int_distribution dis(-99,99);

    std::array<int, 200> numList{};

    std::cout << "Unsorted: ";
    for (int &item : numList) {
        item = dis(gen); // Set a random number between -99 and 99 to the element
        std::cout << item << " ";
    }
    std::cout << std::endl;

    std::sort(numList.begin(), numList.end(), [](int a, int b){return abs(a) <= abs(b);});

    std::cout << "Sorted: ";
    for (int &item : numList) {
        std::cout << item << " ";
    }
    std::cout << std::endl;
}

