//
// Created by Ostrichbeta Yick-Ming Chan on 3/22/22.
//

#ifndef WHUDATASTRUCTUREEXP03_TASK04_H
#define WHUDATASTRUCTUREEXP03_TASK04_H

#include "TaskTemplate.h"

void task04Entry();
const TaskTemplate task04("XML and Sort", "Use a library to load XML into array object.", task04Entry);

#endif //WHUDATASTRUCTUREEXP03_TASK04_H
