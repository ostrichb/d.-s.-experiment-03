//
// Created by Ostrichbeta Yick-Ming Chan on 3/22/22.
//
#include <iostream>
#include <array>
#include <string>

#include "lib/pugixml/pugixml.hpp"

const char *xmlFilePath = "/Users/ostrichb/CLionProjects/WHUDataStructureExp03/data.xml";
void task04Entry(){
    // Include an XML object
    pugi::xml_document docu;
    pugi::xml_parse_result xmlObj = docu.load_file(xmlFilePath);
    return;
}
