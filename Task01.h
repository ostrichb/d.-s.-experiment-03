//
// Created by Ostrichbeta Chan on 2022/03/17.
//

#ifndef WHUDATASTRUCTUREEXP03_TASK01_H
#define WHUDATASTRUCTUREEXP03_TASK01_H

#include "TaskTemplate.h"

void task01Entry();

const TaskTemplate task01("Sorts and predicates", "Make use of the std::sort function with predicates.", task01Entry);

#endif //WHUDATASTRUCTUREEXP03_TASK01_H
